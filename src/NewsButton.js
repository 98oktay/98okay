import React,{Component} from "react";
import {Button} from "reactstrap";


export default class NewsButton extends Component {



    render () {

        const {source,currentSource, clickHandle} = this.props;


        return (
            <Button className="text-white" outline active={source === currentSource} size="sm"
                    onClick={()=>clickHandle(source)} color={this.props.color}>{this.props.children}</Button>

        )
    }
}