import React, {Component} from 'react';
import {Button, Card, CardGroup, CardImg, CardText, CardTitle} from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.css';
import './App.css';
import TimeAgo from "react-timeago";
import NewsButton from './NewsButton';


class App extends Component {



    constructor() {
        super();
        this.state = {
            articles: [],
        };

        this.loadNews = this.loadNews.bind(this);
    }


    componentDidMount() {

        this.loadNews();

    }

    loadNews(source = "cnn") {

        console.log(source);
        let newsAPI = `https://newsapi.org/v1/articles?source=${source}&sortBy=latest&apiKey=afe0871d806c42ceaabf1c9763e8975c`;

        this.setState({
            loading: true,
            source,
            articles: []
        });

        fetch(newsAPI)
            .then((response) => {
                if (response.status >= 400) {
                    throw new Error("Bad response from server");
                }
                return response.json();
            })
            .then((data) => {
                this.setState({articles: data.articles, source, loading: false});
            });

    }

    haberSil(key) {

        let {articles} = this.state;

        articles.splice(key,1);

        this.setState({articles});


    }


    render() {
        let {source, loading} = this.state;
        return (
            <div className="App">
                <header className="App-header text-white">
                    <h1 className="App-title">React Global News</h1>

                    <div>
                        <NewsButton currentSource = {source} source="cnn" clickHandle={this.loadNews} color="danger">CNN</NewsButton>
                        <NewsButton currentSource = {source} source="google-news" color="primary" clickHandle={this.loadNews}>Google News</NewsButton>
                        <NewsButton currentSource = {source} source="national-geographic" clickHandle={this.loadNews} color="warning">National Geographic</NewsButton>

                    </div>

                </header>
                <div className="container">
                    <CardGroup>
                        {this.state.articles.map((item, key) => (
                            <Card key={key} className="m-2">
                                <CardImg width="100%" src={item.urlToImage} alt="Card image cap"/>
                                <div className="card-body">
                                    <CardTitle>{item.title}</CardTitle>
                                    <CardText>{item.description}</CardText>
                                    <CardText>
                                        <small className="text-muted">
                                            <TimeAgo date={item.publishedAt}/>
                                        </small>
                                    </CardText>
                                    <Button color="danger" onClick={()=>this.haberSil(key)}>Sil</Button>
                                </div>
                            </Card>
                        ))}
                    </CardGroup>
                </div>

                {loading && <div className="loading"></div>}


                <footer className="App-Footer bg-dark text-white text-center">
                    <small>Copyright &copy; 2018 Oktay Başkuş</small>
                </footer>
            </div>
        );
    }
}

export default App;
